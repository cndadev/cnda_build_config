#!/bin/bash

# This is the specific set of variables use to build the deployment version of CNDA.

#CURDIR -- this must be set for this script to run
#XNATHOST -- this must be set for this script to run

# Needed by build.properties
export FULL_XNATHOST=`hostname`

if [ -z ${XNATHOST} ]; then
   export XNATHOST=`hostname -s`
fi

# Webapp variables
export TOMCAT_HOME=/usr/share/tomcat6
export APP_NAME=cnda_xnat
export SITE_NAME=CNDA

if [ -z ${CURDIR} ]; then
   export CURDIR="/data/${PROJECT}/home/${PROJECT,,}_build/${XNATHOST}"
fi

# Database info
export DBHOST=${XNATHOST}
export DBUSER=cnda
export DBNAME=cnda
export PW=nrg

# Set the log directory
export LOGDIR=${CURDIR}/logs

# Get current date
export CURDATE=`date +%m/%d/%Y`
export CURDATE_FILE=`echo ${CURDATE} | sed -e 's/\//-/g'`

# Admin email address
export ADMIN_EMAIL=cnda-ops@nrg.wustl.edu
export SMTP_HOST=mail.nrg.wustl.edu

# File containing a list of Puppet-maintained files which should be archived and then restored after the build is complete
export PUPPET_MAINTAINED_FILES=${CURDIR}/puppetMaintainedFiles.txt

# Bitbucket paths
export SSH_BITBUCKET="ssh://hg@bitbucket.org"
export HTTPS_BITBUCKET="https://bitbucket.org"

# Where should the build be run
export BUILDPATH=/data/CNDA/tmp/build_${XNATHOST}

# Deployed pipeline directory
export PIPELINE_HOME=/data/${PROJECT}/pipeline

# Variables to describe what kind of build is desired
# BUILD_TYPE={NEW/EXISTING}
# If EXISTING: Won't attempt to create database.  Will run the update.sh to create the database update sql file.
# If NEW: *CAUTION* Will attempt to drop and create the database.  Won't run the update.sh.
export BUILD_TYPE=EXISTING

# BUILD_PURPOSE={DEV/DEPLOY}
# If DEV: Won't try to puppetize the InstanceSettings.xml file.
#         Will do clone of specified repositories and leave them file system for development.
#         Will run refreshThisRepo.sh on the CNDA_MODULE_REPO
# If DEPLOY:
#         Will try to puppetize the InstanceSettings.xml file.
#         Will download specified repositories as zips and delete them after build.
#         Will not run refreshThisRepo.sh on the CNDA_MODULE_REPO
export BUILD_PURPOSE=DEPLOY
export MODE=HTTPS

# Location of Maven 2 files (needed to recompile the XDAT repository)
#export MAVEN_HOME=/nrgpackages/tools/apache-maven
#export M2_HOME=/nrgpackages/tools/apache-maven
#export MAVENPATH=$M2_HOME/bin
#export MAVEN_OPTS="-Xmx512m -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC"
#export PATH=$PATH:$MAVENPATH

# Param to indicate whether modules should be pulled in on set up or wait until update.sh.
# Leave blank if you want to include modules on setup and update (DCA does this)
# Set to "UPDATE" if you want  to wait until update (CNDA does this).
# Set to "SETUP" if you want to run with modules only at setup.
export RUN_MODULES_STATE=UPDATE

# Default directories 
export WEBAPP_MODULE_DIR_DEFAULT=CUSTOM_WEBAPP_MODULES
export PIPELINE_MODULE_DIR_DEFAULT=CUSTOM_PIPELINE_MODULE

# Bitbucket build repositories (if no merge)
# (_REV lines are optional: can specify changeset or tag)
export BUILD_SCRIPTS=build_scripts
export BUILD_SCRIPTS_OWN=nrg
export BUILD_SCRIPTS_REV=6849ac2e67a3
export CUSTOM_REPO=cnda_xnat_dev
export CUSTOM_REPO_OWN=nrg
export CUSTOM_REPO_REV=b44fd8f80f28
export XNAT_REPO=xnat_builder_1_6dev
export XNAT_REPO_OWN=nrg
export XNAT_REPO_REV=cf0651d47b83
export XNAT_PIPELINE_REPO=pipeline_1_6dev
export XNAT_PIPELINE_REPO_OWN=nrg
export XNAT_PIPELINE_REPO_REV=1786057e2eb8
export CUSTOM_PIPELINE_REPO=cnda_pipeline_dev
export CUSTOM_PIPELINE_REPO_OWN=nrg
export CUSTOM_PIPLINE_REPO_REV=d0c492bc758b
